FROM python:latest
#FROM python:3.9.6-alpine

WORKDIR /code

COPY requirements.txt /code/

RUN pip install -U pip
RUN pip install -r requirements.txt

COPY . /code/

CMD ["gunicorn", "A.wsgi", ":8000"]